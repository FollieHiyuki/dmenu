# dmenu

My build for suckless dmenu

I moded the **Tab/Shift-Tab** keys to behave like **Down/Up**

Patches applied:
  - [border](https://tools.suckless.org/dmenu/patches/border/dmenu-borderoption-20200217-bf60a1e.diff)
  - [center](https://tools.suckless.org/dmenu/patches/center/dmenu-center-20200111-8cd37e1.diff)
  - [fuzzyhighlight](https://tools.suckless.org/dmenu/patches/fuzzyhighlight/dmenu-fuzzyhighlight-4.9.diff)
  - [fuzzymatch](https://tools.suckless.org/dmenu/patches/fuzzymatch/dmenu-fuzzymatch-4.9.diff)
  - [line height](https://tools.suckless.org/dmenu/patches/line-height/dmenu-lineheight-5.0.diff)
  - [morecolor](https://tools.suckless.org/dmenu/patches/morecolor/dmenu-morecolor-20190922-4bf895b.diff)
  - [navhistory](https://tools.suckless.org/dmenu/patches/navhistory/dmenu-navhistory+search-20200709.diff)
  - [numbers](https://tools.suckless.org/dmenu/patches/numbers/dmenu-numbers-4.9.diff)
