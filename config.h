/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
static int centered = 1;                    /* -c option; centers dmenu on screen */
static int min_width = 540;                    /* minimum width when centered */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Sarasa Fixed J:style=Regular:size=13:antialias=true",
	"Iosevka Nerd Font:style=Regular:size=13:antialias=true",
	"Noto Color Emoji:style=Regular:size=13:antialias=true",
	"FiraCode Nerd Font:style=Regular:size=13:antialias=true",
	"monospace:size=13"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#d8dee9", "#2e3440" },
	[SchemeSel] = { "#2e3440", "#81a1c1" },
	[SchemeSelHighlight] = { "#ebcb8b", "#81a1c1" },
	[SchemeNormHighlight] = { "#ebcb8b", "#2e3440" },
	[SchemeOut] = { "#2e3440", "#88c0d0" },
	[SchemeMid] = { "#d8dee9", "#2e3440" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 10;
static unsigned int maxhist    = 64;
static int histnodup           = 1;    /* if 0, record repeated histories */
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 15;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 2;
